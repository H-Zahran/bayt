jQuery(window).load(function() {
  // new WOW().init();
  jQuery("#owl-press").owlCarousel({
    items : 1,
    itemsCustom : false,
    itemsDesktop : [1199,1],
    itemsDesktopSmall : [980,1],
    itemsTablet: [768,1],
    itemsMobile : [479,1],
    singleItem : true,
  });
  jQuery(".owl-hiring").owlCarousel({
    items : 2,
    itemsCustom : false,
    itemsDesktop : [1199,2],
    itemsDesktopSmall : [980,2],
    itemsTablet: [768,2],
    itemsTabletSmall: false,
    itemsMobile : [479,2],
    singleItem : false,
    itemsScaleUp : false,
  });
  jQuery(".owl-testmonials").owlCarousel({
    items : 3,
    itemsCustom : false,
    itemsDesktop : [1199,2],
    itemsDesktopSmall : [980,1],
    itemsTablet: [768,1],
    itemsTabletSmall: false,
    itemsMobile : [479,1],
    singleItem : false,
    itemsScaleUp : false,
  });
});
